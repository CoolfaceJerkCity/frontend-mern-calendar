import { useDispatch, useSelector } from "react-redux";
import { onChecking, onLogin, onLogout, onClearErrorMessage, onLogoutCalendar } from "../store";
import { calendarApi } from '../api';

export const useAuthStore = () => {
    const { status, user, errorMessage } = useSelector(state => state.auth);
    const dispatch = useDispatch();

    const startLogin = async({ email, password }) => {
        dispatch(onChecking());

        try {
            const { data } = await calendarApi.post('/auth', {email, password});
            localStorage.setItem('token', data.token);
            localStorage.setItem('token-init-date', new Date().getTime());

            //Iniciando sesion si todo OK
            dispatch(onLogin({ name: data.name, uid: data.uid}));

        } catch (error) {
            //Cerrando sesion y mostrando error respectivo
            dispatch(onLogout('Credenciales incorrectas'));

            //Limpiando mensaje de error
            setTimeout(() => {
                dispatch(onClearErrorMessage());
            }, 100);
        }
    }

    const startRegister = async({ name, email, password }) => {
        dispatch(onChecking());

        try {
            const { data } = await calendarApi.post('/auth/new', {name, email, password});
            localStorage.setItem('token', data.token);
            localStorage.setItem('token-init-date', new Date().getTime());

            //Iniciando sesion si todo OK
            dispatch(onLogin({ name: data.name, uid: data.uid}));

        } catch (error) {
            //Cerrando sesion y mostrando error respectivo
            dispatch(onLogout( error.response.data?.msg || 'Hable con el administrador'));

            //Mostrando error respectivo
            setTimeout(() => {
                dispatch(onClearErrorMessage());
            }, 100);
        }
    }

    const startLogout = () => {
        localStorage.clear();
        dispatch(onLogout());
        dispatch(onLogoutCalendar());
    }

    const checkAuthToken = async () =>{
        const token = !!(localStorage.getItem("token") || undefined);
        if (!token) return dispatch(onLogout());

        try {
            const { data } = await calendarApi.get('/auth/renew');
            localStorage.setItem('token', data.token);
            localStorage.setItem('token-init-date', new Date().getTime());

            //Iniciando sesion si todo OK
            dispatch(onLogin({ name: data.name, uid: data.uid}));

        } catch (error) {
            console.log({error});

            //Cerrando sesion y mostrando error respectivo
            localStorage.clear();
            dispatch(onLogout());
        }
    }

    return {
        //Propiedades
        status,
        user,
        errorMessage,

        //Metodos
        checkAuthToken,
        startLogin,
        startRegister,
        startLogout
    }
}
