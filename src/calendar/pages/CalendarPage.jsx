import { Calendar } from 'react-big-calendar';
import 'react-big-calendar/lib/css/react-big-calendar.css';

import { Navbar, CalendarEvent, CalendarModal, FabAddButton, FabDeleteButton } from '../';
import { getMessagesES, localizer } from '../../helpers';
import { useState, useEffect } from 'react';
import { useUiStore, useCalendarStore, useAuthStore } from '../../hooks';

export const CalendarPage = () => {
	const { user } = useAuthStore();
	const { openDateModal } = useUiStore();
	const { events, setActiveEvent, startLoadingEvents } = useCalendarStore();
	const [lastView, setLastView] = useState(localStorage.getItem('lastView') || 'week');
	
	const onDoubleClick = () =>{
		openDateModal();
	}

	const onSelect = (event) =>{
		setActiveEvent(event);
	}

	const onViewChanged = (event) => {
		localStorage.setItem('lastView', event);
		setLastView(event);
	}

	const eventStyleGetter = ( event, start, end, isSelected ) => {
		const isMyEvent = (user.uid === event.user._id) || (user.uid === event.user.uid);

		const style = {
			backgroundColor: isMyEvent ? '#347CF7' : '#465660',
			borderRadius: '0px',
			opacity: 0.8,
			color: 'white'
		}
	
		return {
		  	style
		}
	}

	useEffect(() => {
		startLoadingEvents();
	}, []);	

    return (
        <>
          	<Navbar />
			<Calendar
				culture='es'
				defaultView={lastView}
				localizer={localizer}
				events={events}
				startAccessor="start"
				endAccessor="end"
				style={{ height: 'calc(100vh - 80px)' }}
				eventPropGetter={ eventStyleGetter }
				messages={getMessagesES()}
				components={{
					event: CalendarEvent
				}}
				onDoubleClickEvent={onDoubleClick}
				onSelectEvent={onSelect}
				onView={onViewChanged}
			/>

			<CalendarModal />
			<FabAddButton />
			<FabDeleteButton />
        </>
    )
}
