import { useCalendarStore } from '../../hooks';

export const FabDeleteButton = () => {
    const { startDeletingEvent, hasEventSelected } = useCalendarStore();

    const handleClickNew = () => {
        startDeletingEvent();
    }

    return (
        <button className="btn btn-danger fab-delete"
            aria-label="btn-delete"
            onClick={handleClickNew}
            style={{
                display: hasEventSelected ? '' : 'none'
            }}>
            <i className="fas fa-trash-alt"></i>
        </button>
    )
}
