import { useEffect } from 'react';
import { useForm, useAuthStore } from "../../hooks";
import Swal from 'sweetalert2';

const loginFormInitalState = {
    loginEmail: '',
    loginPassword: ''
}

const registerFormInitalState = {
    registerName: '',
    registerEmail: '',
    registerPassword: '',
    registerPassword2: ''
}

export const LoginPage = () => {
    const { loginEmail, loginPassword, onInputChange: onLoginInputChange } = useForm(loginFormInitalState);
    const {  registerName, registerEmail, 
        registerPassword, registerPassword2, 
        onInputChange: onRegisterInputChange } = useForm(registerFormInitalState);
    
    const { startLogin, startRegister, errorMessage } = useAuthStore();

    const onLoginFormSubmit = (e) =>{
        e.preventDefault();
        startLogin({email: loginEmail, password: loginPassword});
    }

    const onRegisterFormSubmit = (e) => {
        e.preventDefault();
        if (registerPassword !== registerPassword2) return Swal.fire('Error en el registro', 'Las contraseñas no son iguales', 'error');
        startRegister({name: registerName, email: registerEmail, password: registerPassword});
    }

    useEffect(() => {
        if(errorMessage !== undefined){
            Swal.fire({icon: 'error', title: 'Error en la autenticación', text: errorMessage});
        }
    }, [errorMessage])


    return (
        <div className="container login-container">
            <div className="row">
                <div className="col-md-6 login-form-1">
                    <h3>Ingreso</h3>
                    <form onSubmit={onLoginFormSubmit}>
                        <div className="form-group mb-2">
                            <input 
                                type="text"
                                className="form-control"
                                placeholder="Correo"
                                name="loginEmail"
                                value={loginEmail}
                                onChange={onLoginInputChange}
                            />
                        </div>
                        <div className="form-group mb-2">
                            <input
                                type="password"
                                className="form-control"
                                placeholder="Contraseña"
                                name="loginPassword"
                                value={loginPassword}
                                onChange={onLoginInputChange}
                            />
                        </div>
                        <div className="d-grid gap-3">
                            <input 
                                type="submit"
                                className="btnSubmit"
                                value="Login" 
                            />
                        </div>
                    </form>
                </div>

                <div className="col-md-6 login-form-2">
                    <h3>Registro</h3>
                    <form onSubmit={onRegisterFormSubmit}>
                        <div className="form-group mb-2">
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Nombre"
                                name="registerName"
                                value={registerName}
                                onChange={onRegisterInputChange}
                            />
                        </div>
                        <div className="form-group mb-2">
                            <input
                                type="email"
                                className="form-control"
                                placeholder="Correo"
                                name="registerEmail"
                                value={registerEmail}
                                onChange={onRegisterInputChange}
                            />
                        </div>
                        <div className="form-group mb-2">
                            <input
                                type="password"
                                className="form-control"
                                placeholder="Contraseña" 
                                name="registerPassword"
                                value={registerPassword}
                                onChange={onRegisterInputChange}
                            />
                        </div>

                        <div className="form-group mb-2">
                            <input
                                type="password"
                                className="form-control"
                                placeholder="Repita la contraseña" 
                                name="registerPassword2"
                                value={registerPassword2}
                                onChange={onRegisterInputChange}
                            />
                        </div>

                        <div className="d-grid gap-3">
                            <input 
                                type="submit" 
                                className="btnSubmit" 
                                value="Crear cuenta" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}