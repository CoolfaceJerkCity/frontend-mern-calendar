import calendarApi from "../../src/api/calendarApi";

describe('Pruebas en calendarApi', () => {
    test('debe de tener la configuración por defecto', () => {
        expect(calendarApi.defaults.baseURL).toBe(process.env.VITE_API_URL);
    });

    test('debe de retonar el x-token en el header de todas las peticiones', async () => {
        const test_token = 'ABC123';

        localStorage.setItem('token', test_token);
        const res = await calendarApi.get('/auth').then((res) => res).catch((res) => res);

        expect(res.config.headers['x-token']).toBe(test_token);
    });
});